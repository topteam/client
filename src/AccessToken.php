<?php

namespace TopThinkCloud;

class AccessToken
{
    protected $raw;
    protected $token;

    protected function __construct($raw, $token)
    {
        $this->raw   = $raw;
        $this->token = $token;
    }

    public function getValue()
    {
        return $this->token;
    }

    public function getRaw($name = null, $default = null)
    {
        if (is_null($name)) {
            return $this->raw;
        } else {
            return isset($this->raw[$name]) ? $this->raw[$name] : $default;
        }
    }

    public function __toString()
    {
        return $this->token;
    }

    public static function make($raw, $tokenName = 'access_token')
    {
        return new self($raw, $raw[$tokenName]);
    }
}

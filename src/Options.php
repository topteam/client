<?php

namespace TopThinkCloud;

use ArrayAccess;
use ReturnTypeWillChange;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Options implements ArrayAccess
{
    protected $value;

    public function __construct($value = [])
    {
        $resolver = new OptionsResolver();

        $resolver->setDefaults([
            'host'          => 'https://www.topthink.com',
            'client_id'     => '',
            'client_secret' => '',
        ]);

        $resolver->setIgnoreUndefined();

        $this->value = $resolver->resolve($value);
    }

    #[ReturnTypeWillChange]
    public function offsetExists($offset)
    {
        return array_key_exists($offset, $this->value);
    }

    #[ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return $this->value[$offset];
    }

    #[ReturnTypeWillChange]
    public function offsetSet($offset, $value)
    {

    }

    #[ReturnTypeWillChange]
    public function offsetUnset($offset)
    {

    }
}

<?php

namespace TopThinkCloud;

use Firebase\JWT\JWK;
use Firebase\JWT\JWT;
use Psr\SimpleCache\CacheInterface;
use TopThinkCloud\HttpClient\Message\ResponseMediator;
use TopThinkCloud\HttpClient\Util\JsonArray;
use TopThinkCloud\HttpClient\Util\QueryStringBuilder;

class OAuth
{
    protected $client;

    protected $options;

    protected $redirectUrl = null;

    /** @var CacheInterface */
    protected $cache = null;

    public function __construct(Client $client, CacheInterface $cache = null)
    {
        $this->client  = $client;
        $this->options = $client->getOptions();
        $this->cache   = $cache;
    }

    /**
     * Set redirect url.
     *
     * @param string $redirectUrl
     *
     * @return $this
     */
    public function setRedirectUrl(string $redirectUrl)
    {
        $this->redirectUrl = $redirectUrl;
        return $this;
    }

    public function getAuthorizeUrl($implicit = true, $options = [])
    {
        $uri = $this->options['host'] . '/oauth/authorize';

        $params = [
            'client_id'     => $this->options['client_id'],
            'response_type' => $implicit ? 'token' : 'code',
            'scope'         => implode(',', $options['scope'] ?? []),
            'state'         => $options['state'] ?? '',
            'confirm'       => $options['confirm'] ?? '',
            'app'           => $options['app'] ?? '',
            'display'       => $options['display'] ?? '',
        ];

        if ($this->redirectUrl) {
            $params['redirect_uri'] = $this->redirectUrl;
        }

        return $uri . QueryStringBuilder::build(array_filter($params));
    }

    public function getAccessToken($type = 'client_credentials', $options = [])
    {
        $params = [
            'client_id'     => $this->options['client_id'],
            'client_secret' => $this->options['client_secret'],
            'grant_type'    => $type,
        ];

        if ($this->redirectUrl) {
            $params['redirect_uri'] = $this->redirectUrl;
        }

        switch ($type) {
            case 'authorization_code':
                $params['code'] = $options['code'];
                break;
            case 'client_credentials':
                break;
        }

        $body    = JsonArray::encode($params);
        $headers = [
            ResponseMediator::CONTENT_TYPE_HEADER => ResponseMediator::JSON_CONTENT_TYPE,
        ];

        $response = $this->client->getHttpClient()->post('/oauth/token', $headers, $body);

        return AccessToken::make(ResponseMediator::getContent($response));
    }

    public function verifyAccessToken(string $accessToken)
    {
        $jwk = $this->getJwk();

        JWT::$leeway = 60;
        return JWT::decode($accessToken, JWK::parseKey($jwk));
    }

    protected function getJwk()
    {
        $key = "{$this->options['host']}-jwk";

        if ($this->cache && $jwk = $this->cache->get($key)) {
            return $jwk;
        }

        $response = $this->client->getHttpClient()->get('/oauth/jwk');
        $jwk      = ResponseMediator::getContent($response);

        if ($this->cache) {
            $this->cache->set($key, $jwk, 24 * 60 * 60);
        }

        return $jwk;
    }
}

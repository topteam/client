<?php

namespace TopThinkCloud;

use Http\Client\Common\HttpMethodsClientInterface;
use Http\Client\Common\Plugin\AddHostPlugin;
use Http\Client\Common\Plugin\HistoryPlugin;
use Http\Client\Common\Plugin\RedirectPlugin;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\SimpleCache\CacheInterface;
use TopThinkCloud\Api\Charge;
use TopThinkCloud\Api\CurrentUser;
use TopThinkCloud\Api\Internal;
use TopThinkCloud\Api\License;
use TopThinkCloud\Api\Notification;
use TopThinkCloud\Api\Passport;
use TopThinkCloud\Api\User;
use TopThinkCloud\Api\Weapp;
use TopThinkCloud\Api\Write;
use TopThinkCloud\HttpClient\Builder;
use TopThinkCloud\HttpClient\Plugin\AcceptJson;
use TopThinkCloud\HttpClient\Plugin\Authentication;
use TopThinkCloud\HttpClient\Plugin\ExceptionThrower;
use TopThinkCloud\HttpClient\Plugin\History;

class Client
{
    /**
     * The private token authentication method.
     *
     * @var string
     */
    public const AUTH_HTTP_TOKEN = 'http_token';

    /**
     * The OAuth 2 token authentication method.
     *
     * @var string
     */
    public const AUTH_OAUTH_TOKEN = 'oauth_token';

    private $options;

    /**
     * The HTTP client builder.
     *
     * @var Builder
     */
    private $httpClientBuilder;

    /**
     * The response history plugin.
     *
     * @var History
     */
    private $responseHistory;

    /** @var CacheInterface */
    protected $cache = null;

    public function __construct(Options $options = null, CacheInterface $cache = null)
    {
        $this->options = $options ?? new Options();
        $this->cache   = $cache;

        $this->responseHistory = new History();

        $builder = new Builder();
        $builder->addPlugin(new ExceptionThrower());
        $builder->addPlugin(new HistoryPlugin($this->responseHistory));
        $builder->addPlugin(new RedirectPlugin());
        $builder->addPlugin(new AcceptJson());
        $builder->addPlugin(new AddHostPlugin($builder->getUriFactory()->createUri($this->options['host'])));
        $this->httpClientBuilder = $builder;
    }

    /**
     * Authenticate a user for all next requests.
     *
     * @param string|null $token Gitlab private token
     * @param string $authMethod One of the AUTH_* class constants
     *
     * @return self
     */
    public function authenticate(string $token = null, string $authMethod = self::AUTH_OAUTH_TOKEN): self
    {
        if (is_null($token)) {
            $token = $this->getClientToken();
        }

        $this->getHttpClientBuilder()->removePlugin(Authentication::class);
        $this->getHttpClientBuilder()->addPlugin(new Authentication($authMethod, $token));
        return $this;
    }

    public function charge()
    {
        return new Charge($this);
    }

    public function currentUser()
    {
        return new CurrentUser($this);
    }

    public function user()
    {
        return new User($this);
    }

    public function write()
    {
        return new Write($this);
    }

    public function weapp()
    {
        return new Weapp($this);
    }

    public function notification()
    {
        return new Notification($this);
    }

    public function license()
    {
        return new License($this);
    }

    public function passport()
    {
        return new Passport($this);
    }

    /**
     * @internal
     */
    public function internal()
    {
        return new Internal($this);
    }

    public function getAuth()
    {
        return new OAuth($this, $this->cache);
    }

    public function getOptions(): Options
    {
        return $this->options;
    }

    /**
     * Get the last response.
     *
     * @return ResponseInterface|null
     */
    public function getLastResponse(): ?ResponseInterface
    {
        return $this->responseHistory->getLastResponse();
    }

    /**
     * Get the HTTP client.
     *
     * @return HttpMethodsClientInterface
     */
    public function getHttpClient(): HttpMethodsClientInterface
    {
        return $this->getHttpClientBuilder()->getHttpClient();
    }

    /**
     * Get the stream factory.
     *
     * @return StreamFactoryInterface
     */
    public function getStreamFactory(): StreamFactoryInterface
    {
        return $this->getHttpClientBuilder()->getStreamFactory();
    }

    /**
     * Get the HTTP client builder.
     *
     * @return Builder
     */
    protected function getHttpClientBuilder(): Builder
    {
        return $this->httpClientBuilder;
    }

    public function getClientToken()
    {
        $key = "{$this->options['host']}-client-token";

        if ($this->cache && $token = $this->cache->get($key)) {
            return $token;
        }

        $oauth = $this->getAuth();
        $token = $oauth->getAccessToken();

        if ($this->cache) {
            $this->cache->set($key, $token, 24 * 60 * 60);
        }

        return $token;
    }
}

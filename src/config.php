<?php

return [
    'client_id'     => env('CLOUD_CLIENT_ID', ''),
    'client_secret' => env('CLOUD_CLIENT_SECRET', ''),
    'host'          => env('CLOUD_HOST', 'https://www.topthink.com'),
];

<?php

namespace TopThinkCloud;

interface OpenUser
{
    public function getOpenid();
}

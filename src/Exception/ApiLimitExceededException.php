<?php

declare(strict_types = 1);

namespace TopThinkCloud\Exception;

class ApiLimitExceededException extends RuntimeException
{
}

<?php

declare(strict_types = 1);

namespace TopThinkCloud\Exception;

class ValidationFailedException extends ErrorException
{
}

<?php

declare(strict_types = 1);

namespace TopThinkCloud\Exception;

class RuntimeException extends \RuntimeException implements ExceptionInterface
{
}

<?php

declare(strict_types = 1);

namespace TopThinkCloud\Exception;

use Http\Client\Exception;

interface ExceptionInterface extends Exception
{
}

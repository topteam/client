<?php

declare(strict_types = 1);

namespace TopThinkCloud\Exception;

class ErrorException extends \ErrorException implements ExceptionInterface
{
}

<?php

namespace TopThinkCloud\Api;

use DomainException;
use think\Request;

class Charge extends AbstractApi
{
    /**
     * 下单
     * @param $data
     * @return array{pay_url:string}
     */
    public function create($data)
    {
        return $this->post('charge', $data);
    }

    /**
     * @param Request|array $data
     * @return void
     */
    public function verify($data)
    {
        if (!is_array($data)) {
            $data = $data->post('', null, null);
        }

        if ($data['sign'] != $this->buildSign($data)) {
            throw new DomainException('签名验证失败');
        }

        return $data;
    }

    /**
     * @param array{trade_no: string, order_no: string} $data
     * @return void
     */
    public function revoke($data)
    {
        return $this->post('charge/revoke', $data);
    }

    /**
     * 查询订单信息
     * @param array{trade_no: string, order_no: string} $data
     * @return array
     */
    public function query($data)
    {
        return $this->get('charge/query', $data);
    }

    protected function buildSign($data)
    {
        unset($data['sign']);
        ksort($data);
        return md5(http_build_query($data) . $this->client->getOptions()['client_secret']);
    }
}

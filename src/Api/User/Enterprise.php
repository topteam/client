<?php

namespace TopThinkCloud\Api\User;

class Enterprise extends UserApi
{
    public function save($data)
    {
        return $this->post("user/{$this->userId}/enterprise", $data);
    }

    public function show()
    {
        return $this->get("user/{$this->userId}/enterprise");
    }
}

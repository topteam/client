<?php

namespace TopThinkCloud\Api\User;

class Coin extends UserApi
{
    /**
     * 增加云币
     * @param $number
     * @param $info
     * @return mixed
     */
    public function inc($number, $info)
    {
        return $this->post("user/{$this->userId}/coin/inc", [
            'number' => $number,
            'info'   => $info,
        ]);
    }

    /**
     * 减少云币
     * @param $number
     * @param $info
     * @return mixed
     */
    public function dec($number, $info)
    {
        return $this->post("user/{$this->userId}/coin/dec", [
            'number' => $number,
            'info'   => $info,
        ]);
    }
}

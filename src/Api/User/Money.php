<?php

namespace TopThinkCloud\Api\User;

class Money extends UserApi
{
    /**
     * 增加余额
     * @param $number
     * @param $info
     * @return mixed
     */
    public function inc($number, $info)
    {
        return $this->post("user/{$this->userId}/money/inc", [
            'number' => $number,
            'info'   => $info,
        ]);
    }

    /**
     * 减少余额
     * @param $number
     * @param $info
     * @return mixed
     */
    public function dec($number, $info)
    {
        return $this->post("user/{$this->userId}/money/dec", [
            'number' => $number,
            'info'   => $info,
        ]);
    }
}

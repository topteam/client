<?php

namespace TopThinkCloud\Api\User;

class Certification extends UserApi
{
    public function save($data)
    {
        return $this->post("user/{$this->userId}/certification", $data);
    }

    public function show()
    {
        return $this->get("user/{$this->userId}/certification");
    }
}

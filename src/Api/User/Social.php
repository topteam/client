<?php

namespace TopThinkCloud\Api\User;

class Social extends UserApi
{
    /**
     * @param array{channel: string, openid: string} $data
     * @return void
     */
    public function save($data)
    {
        return $this->post("user/{$this->userId}/social", $data);
    }
}

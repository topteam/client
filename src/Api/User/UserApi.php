<?php

namespace TopThinkCloud\Api\User;

use TopThinkCloud\Api\AbstractApi;
use TopThinkCloud\Client;

abstract class UserApi extends AbstractApi
{
    protected $userId;

    public function __construct(Client $client, $userId)
    {
        parent::__construct($client);
        $this->userId = $userId;
    }
}

<?php

namespace TopThinkCloud\Api;

use TopThinkCloud\Api\Internal\Api;
use TopThinkCloud\Api\Internal\Sms;

class Internal extends AbstractApi
{
    public function sms()
    {
        return new Sms($this->client);
    }

    public function api()
    {
        return new Api($this->client);
    }
}

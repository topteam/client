<?php

namespace TopThinkCloud\Api;

class Notification extends AbstractApi
{
    /**
     * @param array{to_id:int,from_id:int,content:string,message:string} $data
     * @return mixed
     */
    public function create($data)
    {
        return $this->post('notification', $data);
    }

    /**
     * @param string $token
     * @return string
     */
    public function getScript($token)
    {
        return "<script src='{$this->client->getOptions()['host']}/notification/js' data-token='{$token}' async></script>";
    }

    public function getScriptSrc()
    {
        return "{$this->client->getOptions()['host']}/notification/js";
    }

}

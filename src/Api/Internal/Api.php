<?php

namespace TopThinkCloud\Api\Internal;

use TopThinkCloud\Api\AbstractApi;

class Api extends AbstractApi
{
    public function all($params = [])
    {
        return $this->get('internal/api', $params);
    }

    public function show($name)
    {
        return $this->get("internal/api/{$name}");
    }
}

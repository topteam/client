<?php

namespace TopThinkCloud\Api\Internal;

use TopThinkCloud\Api\AbstractApi;

class Sms extends AbstractApi
{
    public function addSign($data)
    {
        return $this->post('internal/sms/sign', $data);
    }

    public function addTemplate($data)
    {
        return $this->post('internal/sms/template', $data);
    }
}

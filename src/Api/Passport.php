<?php

namespace TopThinkCloud\Api;

class Passport extends AbstractApi
{
    public function login($name)
    {
        return $this->post("passport/login", ['name' => $name]);
    }

    public function user($token)
    {
        return $this->get("passport/user", ['token' => $token]);
    }
}

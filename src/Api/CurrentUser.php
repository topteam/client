<?php

namespace TopThinkCloud\Api;

use TopThinkCloud\Api\CurrentUser\Cert;

class CurrentUser extends AbstractApi
{
    public function info()
    {
        return $this->get('me');
    }

    public function logout()
    {
        return $this->post('me/logout');
    }

    public function cert()
    {
        return new Cert($this->client);
    }
}

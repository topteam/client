<?php

namespace TopThinkCloud\Api;

use TopThinkCloud\Api\User\Certification;
use TopThinkCloud\Api\User\Coin;
use TopThinkCloud\Api\User\Enterprise;
use TopThinkCloud\Api\User\Money;
use TopThinkCloud\Api\User\Social;

class User extends AbstractApi
{
    /**
     * 更新用户信息
     * @param integer $userId
     * @param array{mobile: string, email: string, name: string, avatar: string} $data
     * @return void
     */
    public function update($userId, $data)
    {
        return $this->put("user/{$userId}", $data);
    }

    public function enterprise($userId)
    {
        return new Enterprise($this->client, $userId);
    }

    public function certification($userId)
    {
        return new Certification($this->client, $userId);
    }

    public function social($userId)
    {
        return new Social($this->client, $userId);
    }

    public function coin($userId)
    {
        return new Coin($this->client, $userId);
    }

    public function money($userId)
    {
        return new Money($this->client, $userId);
    }

    /**
     * 增加云币
     * @param $userId
     * @param $number
     * @param $info
     * @return mixed
     * @deprecated
     */
    public function incCoin($userId, $number, $info)
    {
        return $this->post("user/{$userId}/inc_coin", [
            'number' => $number,
            'info'   => $info,
        ]);
    }

    /**
     * 减少云币
     * @param $userId
     * @param $number
     * @param $info
     * @return mixed
     * @deprecated
     */
    public function decCoin($userId, $number, $info)
    {
        return $this->post("user/{$userId}/dec_coin", [
            'number' => $number,
            'info'   => $info,
        ]);
    }
}

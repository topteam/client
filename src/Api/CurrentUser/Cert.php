<?php

namespace TopThinkCloud\Api\CurrentUser;

use TopThinkCloud\Api\AbstractApi;

class Cert extends AbstractApi
{
    public function all($params = [])
    {
        return $this->get("me/cert", $params);
    }

    public function show($id)
    {
        return $this->get("me/cert/{$id}");
    }
}

<?php

namespace TopThinkCloud\Api;

class License extends AbstractApi
{
    public function verify($type, $token)
    {
        return $this->post('license/verify', [
            'type'  => $type,
            'token' => $token,
        ]);
    }

    public function show($type, $id)
    {
        return $this->get("license/{$type}/{$id}");
    }
}

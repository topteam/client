<?php

namespace TopThinkCloud\Api;

class Weapp extends AbstractApi
{
    public function save($data)
    {
        return $this->post("weapp", $data);
    }

    public function show($id)
    {
        return $this->get("weapp/{$id}");
    }

    public function update($id, $data)
    {
        return $this->put("weapp/{$id}", $data);
    }

    public function qrcode($id, $page = null, $scene = null)
    {
        return $this->get("weapp/{$id}/qrcode", ['page' => $page, 'scene' => $scene]);
    }
}

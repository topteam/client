<?php

namespace TopThinkCloud\Api;

class Write extends AbstractApi
{
    public function privatization($name)
    {
        return $this->get("write/privatization/{$name}");
    }
}

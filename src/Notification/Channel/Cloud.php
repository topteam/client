<?php

namespace TopThinkCloud\Notification\Channel;

use TopThinkCloud\Client;
use yunwuxin\Notification;
use yunwuxin\notification\Channel;

class Cloud extends Channel
{
    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function send($notifiable, Notification $notification)
    {
        $data = $this->getMessage($notifiable, $notification);

        $this->client->authenticate();
        $this->client->notification()->create($data);
    }
}

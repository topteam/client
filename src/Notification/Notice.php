<?php

namespace TopThinkCloud\Notification;

use Exception;
use think\facade\Log;
use think\Model;
use think\queue\ShouldQueue;
use TopThinkCloud\Notification\Channel\Cloud;
use TopThinkCloud\OpenUser;
use yunwuxin\Notification;

class Notice extends Notification implements ShouldQueue
{
    public $content;
    public $from;
    public $message;
    public $url;
    public $channel;
    public $app;
    public $sms;

    private function __construct()
    {
    }

    public function channels($notifiable)
    {
        return [Cloud::class];
    }

    public function toCloud($notifiable)
    {
        if ($notifiable instanceof OpenUser) {
            $notifiable = $notifiable->getOpenid();
        } elseif ($notifiable instanceof Model) {
            $pk         = $notifiable->getPk();
            $notifiable = $notifiable->$pk;
        }

        return [
            'to_id'   => $notifiable,
            'content' => $this->content,
            'message' => $this->message,
            'from_id' => $this->from,
            'url'     => $this->url,
            'sms'     => $this->sms,
            'channel' => $this->channel,
            'app'     => $this->app,
        ];
    }

    public static function create(
        string      $content,
                    $from = null,
        string      $message = null,
        string      $url = null,
        string|bool $sms = null,
        string      $channel = null,
        string      $app = null
    )
    {
        if ($from instanceof OpenUser) {
            $from = $from->getOpenid();
        } elseif ($from instanceof Model) {
            $pk   = $from->getPk();
            $from = $from->$pk;
        }

        $notice          = new self();
        $notice->content = $content;
        $notice->from    = $from;
        $notice->message = $message;
        $notice->url     = $url;
        $notice->sms     = $sms;
        $notice->channel = $channel;
        $notice->app     = $app;

        return $notice;
    }

    public function notify($notifiable)
    {
        try {
            \yunwuxin\facade\Notification::send($notifiable, $this);
        } catch (Exception $e) {
            Log::error($e->getMessage());
        }
    }
}

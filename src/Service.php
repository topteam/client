<?php

namespace TopThinkCloud;

use think\Cache;
use think\Config;

class Service extends \think\Service
{
    public function register()
    {
        $this->app->bind(Client::class, function (Config $config, Cache $cache) {
            $options = new Options($config->get('cloud', []));
            return new Client($options, $cache);
        });

        $this->app->bind(OAuth::class, function (Client $client, Cache $cache) {
            return new OAuth($client, $cache);
        });
    }
}

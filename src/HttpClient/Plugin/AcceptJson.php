<?php

namespace TopThinkCloud\HttpClient\Plugin;

use Http\Client\Common\Plugin;
use Http\Promise\Promise;
use Psr\Http\Message\RequestInterface;

final class AcceptJson implements Plugin
{

    public function handleRequest(RequestInterface $request, callable $next, callable $first): Promise
    {
        $request = $request->withHeader('Accept', 'application/json');

        return $next($request);
    }
}

<?php

declare(strict_types = 1);

namespace TopThinkCloud\HttpClient\Plugin;

use TopThinkCloud\Client;
use TopThinkCloud\Exception\RuntimeException;
use Http\Client\Common\Plugin;
use Http\Promise\Promise;
use Psr\Http\Message\RequestInterface;

/**
 * @internal
 */
final class Authentication implements Plugin
{
    /**
     * @var array<string,string>
     */
    private $headers;

    /**
     * @param string $method
     * @param string $token
     *
     * @return void
     */
    public function __construct(string $method, string $token)
    {
        $this->headers = self::buildHeaders($method, $token);
    }

    /**
     * Handle the request and return the response coming from the next callable.
     *
     * @param RequestInterface $request
     * @param callable $next
     * @param callable $first
     *
     * @return Promise
     */
    public function handleRequest(RequestInterface $request, callable $next, callable $first): Promise
    {
        foreach ($this->headers as $header => $value) {
            $request = $request->withHeader($header, $value);
        }

        return $next($request);
    }

    /**
     * Build the headers to be attached to the request.
     *
     * @param string $method
     * @param string $token
     *
     * @return array<string,string>
     * @throws RuntimeException
     *
     */
    private static function buildHeaders(string $method, string $token): array
    {
        $headers = [];

        switch ($method) {
            case Client::AUTH_HTTP_TOKEN:
                $headers['PRIVATE-TOKEN'] = $token;

                break;
            case Client::AUTH_OAUTH_TOKEN:
                $headers['Authorization'] = \sprintf('Bearer %s', $token);

                break;
            default:
                throw new RuntimeException(\sprintf('Authentication method "%s" not implemented.', $method));
        }

        return $headers;
    }
}

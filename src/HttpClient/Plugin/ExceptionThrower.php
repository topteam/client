<?php

declare(strict_types = 1);

namespace TopThinkCloud\HttpClient\Plugin;

use Http\Client\Common\Plugin;
use Http\Promise\Promise;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use TopThinkCloud\Exception\ApiLimitExceededException;
use TopThinkCloud\Exception\AuthenticationException;
use TopThinkCloud\Exception\ExceptionInterface;
use TopThinkCloud\Exception\RetryNeededException;
use TopThinkCloud\Exception\RuntimeException;
use TopThinkCloud\Exception\ValidationFailedException;
use TopThinkCloud\HttpClient\Message\ResponseMediator;

/**
 * @internal
 */
final class ExceptionThrower implements Plugin
{
    /**
     * Handle the request and return the response coming from the next callable.
     *
     * @param RequestInterface $request
     * @param callable $next
     * @param callable $first
     *
     * @return Promise
     */
    public function handleRequest(RequestInterface $request, callable $next, callable $first): Promise
    {
        return $next($request)->then(function (ResponseInterface $response): ResponseInterface {
            $status = $response->getStatusCode();

            if ($status >= 400 && $status < 600) {
                throw self::createException($status, ResponseMediator::getErrorMessage($response) ?? $response->getReasonPhrase());
            }

            return $response;
        });
    }

    /**
     * Create an exception from a status code and error message.
     *
     * @param int $status
     * @param string $message
     *
     * @return ExceptionInterface
     */
    private static function createException(int $status, string $message): ExceptionInterface
    {
        if (400 === $status || 422 === $status) {
            return new ValidationFailedException($message, $status);
        }

        if (429 === $status) {
            return new ApiLimitExceededException($message, $status);
        }

        if (449 === $status) {
            return new RetryNeededException($message, $status);
        }

        if (401 === $status || 403 === $status) {
            return new AuthenticationException($message, $status);
        }

        return new RuntimeException($message, $status);
    }
}

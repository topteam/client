# topthink cloud sdk


## 使用方法

### 用户授权

```php
$options = new \TopThinkCloud\Options([
    'client_id'=>'',
    'client_secret'=>'',
]);

$client = new \TopThinkCloud\Client($options);

$oauth = new \TopThinkCloud\OAuth($client);

//获取授权地址
$url = $oauth->getAuthorizeUrl(false); //false 为PC端授权，true为移动端或者单页应用授权

// 引导用户跳转到授权地址，用户授权后会跳转到回调地址，并且会带上code参数
//使用code换取access_token
$accessToken = $oauth->getAccessToken('authorization_code', [
    'code' => $code,
]);

//使用access_token获取用户信息
$userInfo = $client->authenticate($accessToken)->currentUser()->info();
```



### 服务端调用

```php
$options = new \TopThinkCloud\Options([
    'client_id'=>'',
    'client_secret'=>'',
]);

$client = new \TopThinkCloud\Client($options);

$oauth = new \TopThinkCloud\OAuth($client);

//使用 Client credentials grant 获取 access_token
$accessToken = $oauth->getAccessToken();

//使用access_token调用api
//创建订单
$result = $client->authenticate($accessToken)->charge()->create([
    'order_no'   => '', //订单号，必填
    'subject'    => '', //订单标题，必填
    'amount'     => '', //订单金额，必填，单位：分
    'user_id'    => '', //用户ID,为顶想云用户的ID，选填 
    'return_url' => '', //支付成功后跳转的地址，选填
    'notify_url' => '', //支付成功后的回调地址，选填
    'revoke_url' => '', //订单撤销后的回调地址，选填
]);

//可以弹出一个新窗口 打开 $result['pay_url'] 进行支付
//关于回调地址的说明
//回调会以post的方式发送数据到回调地址，数据格式为
// {
//     "trade_no": "xxxxxx",//交易号
//     "order_no": "xxxxx",//订单号
//     "amount": 10000, //支付金额
//     "sign": "xxx", //签名  可以使用 $client->charge()->verify($data) 来验证签名
// }

//查询订单
$result = $client->authenticate($accessToken)->charge()->query([
    'order_no'   => '', //订单号
    'trade_no'    => '', //交易号，二者选一
]);
```
